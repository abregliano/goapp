package main

import (
	"context"
	"fmt"
	"log"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
)

var ctx = context.Background()

type Record struct {
	Nome    string `json:"nome"`
	Cognome string `json:"cognome"`
	Email   string `json:"email"`
}

func main() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "foobared", // no password set
		DB:       0,          // use default DB
	})

	pong := rdb.Ping(ctx)
	if pong.Err() != nil {
		log.Fatal(pong.Err())
	}

	var r = Record{
		Nome:    "Pippo",
		Cognome: "Baudo",
		Email:   "pippo.baudo@email.it",
	}

	id := uuid.New().ID()
	fmt.Println(id)

	idstr := uuid.New().String()
	fmt.Println(idstr)
	v3 := uuid.MustParse(idstr)
	nn := v3.ClockSequence()
	fmt.Println("n: ", nn)

	res := rdb.HSet(
		ctx, idstr,
		"nome", r.Nome,
		"cognome", r.Cognome,
		"email", r.Email,
	)

	n, err := res.Result()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(n)

	// Output: key value
	// key2 does not exist
}
