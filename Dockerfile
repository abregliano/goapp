FROM golang:1.22-alpine as build

WORKDIR /go/src/app

COPY . .

RUN go build -o app

FROM alpine

COPY --from=build /go/src/app/app /usr/local/bin/app

ENTRYPOINT ["/usr/local/bin/app"]
